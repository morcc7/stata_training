---
### A. 北京专场 (2018.7.23日-8.2日)

> **时间地点：** 2018年7.23 日-8.2 日，首都师范大学          
> **授课教师：** 连玉君  (中山大学)     
> **课程资料：** [在线浏览课程大纲-PDF](https://gitee.com/arlionn/stata_training/raw/master/%E8%BF%9E%E7%8E%89%E5%90%9B_2018%E6%9A%91%E5%81%87_%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D%E5%8F%8A%E5%A4%A7%E7%BA%B2.pdf)  || [交通指南](https://gitee.com/arlionn/stata_training/raw/master/files/2018%E5%B9%B47%E6%9C%88Stata%E6%9A%91%E6%9C%9F%E7%8F%AD%E4%BA%A4%E9%80%9A%E4%BD%8F%E5%AE%BF%E6%8C%87%E5%8D%97.pdf)       
> **报名咨询：** [初级班 (7.23-25)](http://www.peixun.net/view/308_detail.html)  || [高级班 (7.27-29)](http://www.peixun.net/view/308_detail.html)  || [论文班 (7.31-8.2)](http://www.peixun.net/view/1135.html)     
> **课程要点：** 全面提升：完整的Stata学习架构、涵盖主要研究方法、论文班精讲五篇论文(综合应用)

### B、山西专场 (2018年8.3日-8.8日)
> **时间地点：** 2018 年 8.3 日- 8.8 日，山西财经大学     
> **授课教师：** 连玉君  (中山大学)、黄河泉 (淡江大学)     
> **课程链接：** [实证研究方法与Stata应用](https://mp.weixin.qq.com/s?__biz=MzU5MjYxNTgwMg==&mid=2247483668&idx=1&sn=98651b92ef4a0e87ed6b700701bdd328&chksm=fe1c4101c96bc8171299c454b71802fdc6078fac9d4c5e500655d8e82633bae316181240a513&mpshare=1&scene=1&srcid=0529RYga0qi6xmUwqYuQPQ53#rd)      
> **课程要点：** 随机边界分析(SFA)，倍分法(DID)，断点回归分析(RDD)，合成控制法

### C、广东专场：第二届 Stata 用户大会 (2018年8.21日-23日)
> **时间地点：** 2018 年 8.21 日- 8.23 日，广东 - 顺德职业技术学院     
> **授课教师：** 连玉君  (中山大学)     
> **课程链接：** [Stata计量与实证分析(连玉君)](http://www.uone-tech.cn/stataMarketing2/training.html)  >>> [课程资料](http://www.uone-tech.cn/sd-stata20180821lyj.html)     
> **课程要点：** 模型设定和研究设计、面板数据模型、自抽样和蒙特卡洛模拟、精讲三篇论文

---
### 课程特色

1.  **渔非鱼。** 体系完整，注重模型设定思路、研究设计和结果的解释。
2.  **电子板书。** 全程电子板书，听课更专注，复习更高效；【**下载**】[连玉君-FE-板书](https://gitee.com/arlionn/stata_training/raw/master/Stata%E6%9D%BF%E4%B9%A6/Stata%E6%9D%BF%E4%B9%A6-FE-version2.pdf)
3.  **可重现-可移植。** 提供全套课件，包括dofile、范例数据、自编程序，课程中的估计方法和代码都可以快速移植到自己的论文中。【**下载**】[A1_intro.do](https://gitee.com/arlionn/stata_training/raw/master/dofile-data/A1_intro.pdf),  [A6_DID.do](https://gitee.com/arlionn/stata_training/raw/master/dofile-data/A6_DID.pdf)
4.  **注重方法的合理搭配。** 基于每篇文章的重现，重点讲解如下几个论文写作中普遍面临但又非常棘手的问题：研究设计如何开展？稳健性检验包括哪些内容？主要有哪些方法和途径？内生性问题如何确认和解决？

---
### 课前预习

- [连玉君-Stata视频教程](http://www.peixun.net/author/3.html)
- [连玉君-优酷Stata公开课](http://i.youku.com/arlion)
- [Stata连享会-简书](https://www.jianshu.com/u/69a30474ef33)
- [Stata连享会-知乎](https://www.zhihu.com/people/arlionn)

>![连老师授课中](https://upload-images.jianshu.io/upload_images/7692714-b143eedbc2863f94.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-bb790ce3e251c1a3.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")
